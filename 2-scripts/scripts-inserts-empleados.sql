-- Paises -- 
insert into paises (codigo, nombre, dominio) values ('005', 'Colombia', 'cidenet.com.co');
insert into paises (codigo, nombre, dominio) values ('057', 'Estados Unidos', 'cidenet.com.us');
commit;

-- Tipos de Identificación  --
insert into tipos_identificacion (tipo_identificacion, descripcion) values ('CC', 'Cédula de Ciudadanía');
insert into tipos_identificacion (tipo_identificacion, descripcion) values ('CE', 'Cédula de Extranjería');
insert into tipos_identificacion (tipo_identificacion, descripcion) values ('PS', 'Pasaporte');
insert into tipos_identificacion (tipo_identificacion, descripcion) values ('PE', 'Permiso Especial');
commit;

-- Áreas  --
insert into areas (nombre) values ('Administración');
insert into areas (nombre) values ('Financiera');
insert into areas (nombre) values ('Compras');
insert into areas (nombre) values ('Operación');
insert into areas (nombre) values ('Talento Humano');
insert into areas (nombre) values ('Servicios Varios');
insert into areas (nombre) values ('Infraestructura');
commit;

-- Esstados de Empleados -- 
insert into estados_empleados (cod_estado, descripcion) values ('ACT', 'Activo');
insert into estados_empleados (cod_estado, descripcion) values ('INA', 'Inactivo');
insert into estados_empleados (cod_estado, descripcion) values ('VAC', 'Vacaciones');
commit;

