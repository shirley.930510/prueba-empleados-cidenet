create sequence paises_sqc start with 1 increment by 1 ;
create table paises(
	id_pais					number(4)		default paises_sqc.nextval
											constraint paises_id_pais_pk				primary key
											constraint paises_id_pais_nn				not null,
	codigo					varchar2(5)		constraint paises_codigo_un					unique
											constraint paises_codigo_nn					not null,
	nombre					varchar2(50)	constraint paises_nombre_nn					not null,
	dominio					varchar2(50)	constraint paises_dominio_nn				not null
);
comment on table paises				is 'Entidad que almacena los paises';
comment on column paises.id_pais	is 'Identificador de la tabla';
comment on column paises.codigo		is 'Código del país';
comment on column paises.nombre		is 'Nombre del pais';
comment on column paises.dominio	is 'Dominio Correo';

create table tipos_identificacion (
	tipo_identificacion		varchar2(3)		constraint tip_idnt_tip_identificacion_pk	primary key
											constraint tip_idnt_tip_identificacion_nn	not null,
	descripcion				varchar2(30)	constraint tip_idnt_descrpcion_nn			not null

);
comment on table tipos_identificacion 						is  'Tabla que almacena los diferentes tipo de identificación. Ej: Cedula, Pasaporte, etc';
comment on column tipos_identificacion.tipo_identificacion	is 'Código de los tipo de identificación';
comment on column tipos_identificacion.descripcion			is 'Descripción del tipo de identificación';

create sequence areas_sqc start with 1 increment by 1 ;
create table areas (
	id_area					number(2)		default areas_sqc.nextval
											constraint areas_id_area_pk					primary key
											constraint areas_id_area_nn					not null,
	nombre					varchar2(50)	constraint areas_nombre_nn					not null

);
comment on table areas 				is 'Tabla que almacena las áreas de la organización';
comment on column areas.id_area		is 'Identificador de la tabla';
comment on column areas.nombre		is 'Nombre del área';

create table estados_empleados (
	cod_estado				varchar2(3)		constraint estado_empl_cod_estado_empl_pk	primary key
											constraint estado_empl_cod_estado_empl_nn	not null,
	descripcion				varchar2(30)	constraint estado_empl_descrpcion_nn		not null

);
comment on table estados_empleados 						is 'Tabla que almacena los diferentes estado que puede tomar un empelado. Ej: Activo, Inactivo, En vacaciones, etc';
comment on column estados_empleados.cod_estado			is 'Código del estado del empleado';
comment on column estados_empleados.descripcion			is 'Descripción del estado del empleado';

create sequence empleados_sqc start with 1 increment by 1;
create table empleados(
	id_empleado				number(10)		constraint empleados_id_empleado_pk			primary key
											constraint empleados_id_empleado_nn			not null,
	primer_apellido			varchar2(20)	constraint empleados_primer_apellido_nn		not null,
	segundo_apellido		varchar2(20)	constraint empleados_segundo_apellido_nn	not null,
	primer_nombre			varchar2(20)	constraint empleados_primer_nomnbre_nn		not null,	
	otros_nombres			varchar2(50),
	id_pais					number(4)		constraint empleados_id_pais_fk				references paises (id_pais)
											constraint empleados_id_pais_nn				not null,
	tipo_identificacion		varchar2(3)		constraint empleados_tip_idetificacion_fk	references tipos_identificacion (tipo_identificacion)
											constraint empleados_tip_idetificacion_nn	not null,
	identificacion			varchar2(20)	constraint empleados_identificacion_nn		not null,
	email					varchar2(300)	constraint empleados_email_nn				unique
											constraint empleados_email_un				not null,
	fecha_ingreso			date			constraint empleados_fecha_ingreso_nn		not null,
	id_area					number(2)		constraint empleados_id_area_fk				references areas (id_area)
											constraint empleados_id_area_nn				not null,
	cod_estado				varchar2(3)		default 'ACT'
											constraint empleados_cod_estado_fk			references estados_empleados (cod_estado)
											constraint empleados_cod_estado_nn			not null,
	fecha_registro			timestamp		default systimestamp
											constraint empleados_fecha_registro_nn		not null,
	fecha_modificacion		timestamp,
	constraint empleados_tip_idn_identf_un unique (tipo_identificacion, identificacion)
	
);
comment on table empleados 						is 'Tabla que almacena la información básica de los empleados';
comment on column empleados.id_empleado			is 'Identificador de la tabla';
comment on column empleados.primer_apellido		is 'Primer Apellido';
comment on column empleados.segundo_apellido	is 'Segundo Apellido';
comment on column empleados.primer_nombre		is 'Primer Nombre';
comment on column empleados.otros_nombres		is 'Otros Nombres';
comment on column empleados.id_pais				is 'Identificador del pais al que pertenece el empleado';
comment on column empleados.tipo_identificacion	is 'Tipo de identificacion';
comment on column empleados.identificacion		is 'Numero de Identificación';
comment on column empleados.email				is 'Email asignado al empleado';
comment on column empleados.fecha_ingreso		is 'Fecha de ingreso del empleado';
comment on column empleados.id_area				is 'Identificador del área al que pertenece el empleado';
comment on column empleados.cod_estado			is 'Código del estado del empleado';
comment on column empleados.fecha_registro		is 'Fecha de registro del empleado';
comment on column empleados.fecha_modificacion	is 'Fecha de última modificación del empleado';


create view v_empleados as 
   select a.id_empleado
		, a.primer_apellido
		, a.segundo_apellido
		, a.primer_nombre
		, a.otros_nombres
		, a.id_pais
		, b.nombre					nombre_pais
		, a.tipo_identificacion
		, c.descripcion				desc_tipo_identificacion
		, a.identificacion
		, a.email
		, a.fecha_ingreso
		, a.id_area
		, d.nombre					nombre_area
		, a.cod_estado
		, e.descripcion				descripcion_estado
		, a.fecha_registro
		, a.fecha_modificacion
	 from empleados					a
	 join paises					b on a.id_pais 				= b.id_pais
	 join tipos_identificacion		c on a.tipo_identificacion	= c.tipo_identificacion
	 join areas						d on a.id_area				= d.id_area
	 join estados_empleados			e on a.cod_estado			= e.cod_estado
;
/