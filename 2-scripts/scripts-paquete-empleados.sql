create or replace package pkg_empleados as

/*
-- Paquete que contiene procedimiento y funciones para el CRUD de empleados -- 
-- Creado: 18-03-2021
-- Autor: Shirley Romero
*/
	-- Procedimiento que valida la fecha de ingreso 
	procedure prc_validar_fecha_ingreso (p_fecha_ingreso				in date
									   , s_ind_fecha_ingreso_valida		out varchar2
									   , s_mensaje_respuesta			out varchar2);

	-- Procedimiento para validar las cadenas de nombres y apellidos
	procedure prc_validar_nombre_apellidos (p_cadena					in varchar2
										  , p_longitud_maxima			in number
										  , s_ind_cadena_valida			out varchar2
										  , s_mensaje_respuesta			out varchar2);

	-- Procedimiento para validar la identificacion
	procedure prc_validar_identificacion (p_cadena						in varchar2
										, p_longitud_maxima				in number
										, s_ind_cadena_valida			out varchar2
										, s_mensaje_respuesta			out varchar2);

	-- Procedimiento que retorna el email de un nuevo empleado 
	procedure prc_crear_email_empleado (p_primer_apellido				in varchar2
									  , p_primer_nombre					in varchar2
									  , p_id_pais						in number
									  , s_email							out empleados.email%type
									  , s_mensaje_respuesta				out varchar2);

	-- Procedimiento para crea un empleado y retorna el id del nuevo empleado --
	procedure prc_crear_empleados (p_primer_apellido					in varchar2
								 , p_segundo_apellido					in varchar2
								 , p_primer_nombre						in varchar2
								 , p_otros_nombres						in varchar2
								 , p_id_pais							in number
								 , p_tipo_identificacion				in varchar2
								 , p_identificacion						in varchar2
								 , p_fecha_ingreso						in date
								 , p_id_area							in number
								 , s_id_empleado						out number
								 , s_mensaje_respuesta					out varchar2);

	-- Procedimiento para editar un empleado --
	procedure prc_editar_empleados (p_id_empleado						in empleados.email%type
								  , p_primer_apellido					in varchar2	default null
								  , p_segundo_apellido					in varchar2 default null
								  , p_primer_nombre						in varchar2 default null
								  , p_otros_nombres						in varchar2 default null
								  , p_id_pais							in number   default null
								  , p_tipo_identificacion				in varchar2 default null
								  , p_identificacion					in varchar2 default null
								  , p_fecha_ingreso						in date		default null
								  , p_id_area							in number	default null
								  , s_mensaje_respuesta					out varchar2);

	-- Procedimiento para consultar todos los empleados --
	procedure prc_consulta_empleados (s_empleados						out sys_refcursor
								    , s_mensaje_respuesta				out varchar2);

	-- Procedimiento para empleados por parametros --
	procedure prc_consulta_empleados_parame (p_primer_apellido			in varchar2	default null
										   , p_segundo_apellido			in varchar2 default null
										   , p_primer_nombre			in varchar2 default null
										   , p_otros_nombres			in varchar2 default null
										   , p_tipo_identificacion		in varchar2 default null
										   , p_identificacion			in varchar2 default null
										   , p_id_pais					in number   default null
										   , p_email					in varchar2 default null
										   , p_cod_estado				in varchar2	default null
										   , s_empleados				out sys_refcursor
										   , s_mensaje_respuesta		out varchar2);

	-- Procedimiento para eliminar un empleado --
	procedure prc_eliminar_empleado ( p_id_empleado						in empleados.email%type
									, s_mensaje_respuesta				out varchar2);
end pkg_empleados;
/


create or replace package body pkg_empleados as
/*
-- Paquete que contiene procedimiento y funciones para el CRUD de empleados -- 
-- Creado: 18-03-2021
-- Autor: Shirley Romero
*/

	-- Procedimiento que valida la fecha de ingreso 
	procedure prc_validar_fecha_ingreso (p_fecha_ingreso				in date
									   , s_ind_fecha_ingreso_valida		out varchar2
									   , s_mensaje_respuesta			out varchar2) as
	begin
		if trunc(p_fecha_ingreso) > trunc(sysdate) then 
			s_ind_fecha_ingreso_valida	:= 'N';
			s_mensaje_respuesta			:= 'Fecha de ingreso no valida, la fecha de ingreso no puede ser superior a la fecha actual';
		elsif trunc(p_fecha_ingreso) <  add_months (sysdate,-1) then
			s_ind_fecha_ingreso_valida	:= 'N';
			s_mensaje_respuesta			:= 'Fecha de ingreso no valida, la fecha de ingreso solo puede ser mas un mes menos a la fecha actual';
		else
			s_ind_fecha_ingreso_valida	:= 'S';
			s_mensaje_respuesta			:= 'Fecha valida';
		end if;
	
	end prc_validar_fecha_ingreso;

	-- Procedimiento que nombres y apellidos
	procedure prc_validar_nombre_apellidos (p_cadena					in varchar2
										  , p_longitud_maxima			in number
										  , s_ind_cadena_valida			out varchar2
										  , s_mensaje_respuesta			out varchar2) as 
	v_cadena	varchar2(50)	:= replace(p_cadena, ' ', '');
	begin 
		s_ind_cadena_valida		:= 'S';
		
		if length(v_cadena) > p_longitud_maxima then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, supera la cantidad máxima de caracteres';
			return;
		end if;
		
		-- Se valida que la cadena solo contenta caracteres de A a la Z
		if length(regexp_replace(v_cadena,' *[A-Z]*')) > 0 or length(regexp_replace(v_cadena,' *[A-Z]*')) is not null then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, contiene caracteres no validos';
			return;
		end if;
		
		-- Se valida que todos los caracteres esten en mayuscula
		if not regexp_like (v_cadena,'^[[:upper:]]+$') then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, todos los caracteres deben estar en mayuscula';
			return;
		end if;
		
		-- Se valida que la cadena no contenga una Ñ
		if (regexp_instr (v_cadena, 'Ñ')) > 0 then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, contiene Ñ que es un caracter no valido';
			return;
		end if;
			
		-- Se valida que la cadena no contenga acentos
		if regexp_like(v_cadena,'[áéíóúüÁÉÍÓÚÜ]') then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, contiene acentos';
			return;
		end if;
	end prc_validar_nombre_apellidos;


	procedure prc_validar_identificacion (p_cadena						in varchar2
										, p_longitud_maxima				in number
										, s_ind_cadena_valida			out varchar2
										, s_mensaje_respuesta			out varchar2) as 
	begin
		if length(p_cadena) > p_longitud_maxima then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, supera la cantidad máxima de caracteres';
			return;
		end if;
		
		if not regexp_like(p_cadena, '^([A-Za-z0-9]+[A-Za-z0-9-]+[A-Za-z0-9]+)$') then
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, contiene caracteres no validos';
			return;
		end if;
		
		-- Se valida que la cadena solo contenta caracteres de A a la Z
		if length(regexp_replace(p_cadena,' *[A-Za-z0-9]*')) > 0 or length(regexp_replace(p_cadena,' *[A-Za-z0-9]*')) is not null then 
			s_ind_cadena_valida		:= 'N';
			s_mensaje_respuesta		:= 'no se cumple con las condiciones, contiene caracteres no validos';
			return;
		end if;
		
	end prc_validar_identificacion;

	-- Procedimiento que retorna el email de un nuevo empleado -- 
	procedure prc_crear_email_empleado (p_primer_apellido			in varchar2
									  , p_primer_nombre				in varchar2
									  , p_id_pais					in number
									  , s_email						out empleados.email%type
									  , s_mensaje_respuesta			out varchar2) is 
	v_dominio		paises.dominio%type;
	v_count_email	number	:= 0;
	
	begin
		-- Se consulta el dominio correspondiente de acuerdo al país de empleo
		begin
			select dominio
			  into v_dominio
			  from paises 
			 where id_pais 		= p_id_pais;
		exception
			when no_data_found then 
				s_email				:= null;
				s_mensaje_respuesta	:= 'NO se encontro información del dominio para el país ingresado';
				return;
			when others then 
				s_email				:= null;
				s_mensaje_respuesta	:= 'Error al consultar la información del dominio para el país ingresado. ' || sqlerrm;
				return;
		end;
		
		if v_dominio is null then 
			s_email				:= null;
			s_mensaje_respuesta	:= 'No existe dominio parametrizado para el pais ingresado';
			return;
		end if;
		
		s_email	:= p_primer_nombre		|| '.'
				|| p_primer_apellido	|| '@'
				|| v_dominio;
				
		s_email	:= lower(replace(s_email, ' ', ''));
		
		-- Se consulta si ya existe un email igual en la tabla de empleados
		begin
			select nvl(max(id),0)
			  into v_count_email
			  from ( select regexp_replace(email,'*[A-Za-z-.-@]*') id
					   from empleados    a
					  where primer_nombre	= p_primer_nombre
					    and primer_apellido	= p_primer_apellido
					    and id_pais			= p_id_pais);
			   
			-- dbms_output.put_line('v_count_email: *' || v_count_email || '*');
		exception
			when others then
				s_email				:= null;
				s_mensaje_respuesta	:= 'Error al consultar emial de los empleado. ' || sqlerrm;
				return;
		end;
		
		if v_count_email > 0 then
			s_email	:= p_primer_nombre				|| '.'
					|| p_primer_apellido			|| '.'
					|| to_number(v_count_email + 1)	|| '@'
					|| v_dominio;
			s_email	:= lower(replace(s_email, ' ', ''));
		end if;
		if length (s_email) > 300 then 
			s_email				:= null;
			s_mensaje_respuesta	:= 'El Email supera el máximo de caracteres permitidos';
			return;
		else
			s_email	:= lower(s_email);
		end if;
		
		dbms_output.put_line('s_email: ' || s_email);
	end prc_crear_email_empleado;

	-- Procedimiento que crea un empleado y retorna el id del nuevo empleado --
	procedure prc_crear_empleados (p_primer_apellido			in varchar2
								 , p_segundo_apellido			in varchar2
								 , p_primer_nombre				in varchar2
								 , p_otros_nombres				in varchar2
								 , p_id_pais					in number
								 , p_tipo_identificacion		in varchar2
								 , p_identificacion				in varchar2
								 , p_fecha_ingreso				in date
								 , p_id_area					in number
								 , s_id_empleado				out number
								 , s_mensaje_respuesta			out varchar2) as
								 
	v_email								empleados.email%type;
	v_ind_fecha_ingreso_valida			varchar2(1);
	v_msj_repuesta_fecha_ingreso		varchar2(100);
	v_ind_cadena_valida					varchar2(1);
	v_count_empleados					number		:= 0;
	
	begin
		
		-- Se valida la fecha de ingreso
		begin 
			pkg_empleados.prc_validar_fecha_ingreso (p_fecha_ingreso				=> p_fecha_ingreso
												   , s_ind_fecha_ingreso_valida		=> v_ind_fecha_ingreso_valida
												   , s_mensaje_respuesta			=> v_msj_repuesta_fecha_ingreso);
			if v_ind_fecha_ingreso_valida = 'N' then 
				s_mensaje_respuesta			:= v_msj_repuesta_fecha_ingreso;
				return;
			end if;
		end;
		
		-- Se valida la cadena del primer apellido
		begin
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_primer_apellido
													  , p_longitud_maxima			=> 20
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'El primer apellido ' || s_mensaje_respuesta;
				return;
			end if;
		end;
		
		-- Se valida la cadena del segundo apellido
		begin
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_segundo_apellido
													  , p_longitud_maxima			=> 20
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'El segundo apellido ' || s_mensaje_respuesta;
				return;
			end if;
		end;
		
		-- Se valida la cadena del primer nombre
		begin
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_primer_nombre
													  , p_longitud_maxima			=> 20
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'El primer nombre ' || s_mensaje_respuesta;
				return;
			end if;
		end;
		
		-- Se valida la cadena del otros nombres
		begin
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_otros_nombres
													  , p_longitud_maxima			=> 50
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'Otros nombres ' || s_mensaje_respuesta;
				return;
			end if;
		end;
		
		-- Se valida la cadena de la identificacion
		begin
			pkg_empleados.prc_validar_identificacion (p_cadena					=> p_identificacion
													, p_longitud_maxima			=> 20
													, s_ind_cadena_valida		=> v_ind_cadena_valida
													, s_mensaje_respuesta		=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'La identificación ' || s_mensaje_respuesta;
				return;
			end if;
		end;
		
		-- Se consulta si ya existe otro empleado con el tipo e identificacion del nuevo empleado
		begin
			select count(1)
			  into v_count_empleados
			  from empleados 
			 where tipo_identificacion		= p_tipo_identificacion
			   and identificacion			= p_identificacion;
			if v_count_empleados  > 0 then
				s_mensaje_respuesta	:= 'Ya existe un empleado con el tipo de identificacion y la identificacion ingresada';
				return;
			end if;
		end;

		-- Se genera el email del nuevo empelado
		begin
			pkg_empleados.prc_crear_email_empleado (p_primer_apellido			=> p_primer_apellido
												  , p_primer_nombre				=> p_primer_nombre
												  , p_id_pais					=> p_id_pais
												  , s_email						=> v_email
												  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_email is null then 
				return;
			end if;
		end;
																 
		-- Se registra el nuevo empleado
		begin
			s_id_empleado	:= empleados_sqc.nextval;
			insert into empleados (id_empleado,				primer_apellido,		segundo_apellido
								 , primer_nombre,			otros_nombres,			id_pais
								 , tipo_identificacion,		identificacion,			email
								 , fecha_ingreso,			id_area,				cod_estado)
						   values (s_id_empleado,			p_primer_apellido,		p_segundo_apellido
								 , p_primer_nombre,			p_otros_nombres,		p_id_pais
								 , p_tipo_identificacion,	p_identificacion,		v_email
								 , p_fecha_ingreso,			p_id_area,				'ACT');
			commit;
		exception
			when others then 
				s_mensaje_respuesta	:= 'Error al crear el empleado ' || p_primer_apellido || ' ' || p_primer_nombre || '. Error: ' || sqlerrm;
				rollback;
				return;
		end;

	exception
		when others then 
			s_mensaje_respuesta	:= 'Error al crear el empleado ' || p_primer_apellido || ' ' || p_primer_nombre || '. Error: ' || sqlerrm;
			rollback;
			return;
	end prc_crear_empleados;

	-- Procedimiento para editar un empleado --
	procedure prc_editar_empleados (p_id_empleado						in empleados.email%type
								  , p_primer_apellido					in varchar2	default null
								  , p_segundo_apellido					in varchar2 default null
								  , p_primer_nombre						in varchar2 default null
								  , p_otros_nombres						in varchar2 default null
								  , p_id_pais							in number   default null
								  , p_tipo_identificacion				in varchar2 default null
								  , p_identificacion					in varchar2 default null
								  , p_fecha_ingreso						in date		default null
								  , p_id_area							in number	default null
								  , s_mensaje_respuesta					out varchar2) as
								 
	t_empleados							empleados%rowtype;
	v_ind_cadena_valida					varchar2(1);
	v_count_empleados					number		:= 0;	
	v_email								empleados.email%type;
	
	begin
		
		-- Se valida si el empleado ingresado existe
		begin 
			select *
			  into t_empleados
			  from empleados
			 where id_empleado = p_id_empleado;
		exception
			when no_data_found then 
				s_mensaje_respuesta	:= 'No se encontro información para el empleado ingresado';
				return;
			when others then
				s_mensaje_respuesta	:= 'Error al consultar el empleado ' || sqlerrm;
				return;
		end;
			
		-- Se valida la cadena del primer apellido
		if p_primer_apellido is not null and t_empleados.primer_apellido != p_primer_apellido then 
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_primer_apellido
													  , p_longitud_maxima			=> 20
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'El primer apellido ' || s_mensaje_respuesta;
				return;
			end if;
		end if;
		
		-- Se valida la cadena del segundo apellido
		if p_segundo_apellido is not null and t_empleados.segundo_apellido != p_segundo_apellido then 
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_segundo_apellido
													  , p_longitud_maxima			=> 20
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'El segundo apellido ' || s_mensaje_respuesta;
				return;
			end if;
		end if;
		
		-- Se valida la cadena del primer nombre
		if p_primer_nombre is not null and t_empleados.primer_nombre != p_primer_nombre then 
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_primer_nombre
													  , p_longitud_maxima			=> 20
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'El primer nombre ' || s_mensaje_respuesta;
				return;
			end if;
		end if;
		
		-- Se valida la cadena del otros nombres
		if p_otros_nombres is not null and t_empleados.otros_nombres != p_otros_nombres then 
			pkg_empleados.prc_validar_nombre_apellidos (p_cadena					=> p_otros_nombres
													  , p_longitud_maxima			=> 50
													  , s_ind_cadena_valida			=> v_ind_cadena_valida
													  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'Otros nombres ' || s_mensaje_respuesta;
				return;
			end if;
		end if;
		
		-- Se valida la cadena de la identificacion
		if p_identificacion is not null and t_empleados.identificacion != p_identificacion then 
			pkg_empleados.prc_validar_identificacion (p_cadena					=> p_identificacion
													, p_longitud_maxima			=> 20
													, s_ind_cadena_valida		=> v_ind_cadena_valida
													, s_mensaje_respuesta		=> s_mensaje_respuesta);
			if v_ind_cadena_valida = 'N' then 
				s_mensaje_respuesta	:= 'La identificación ' || s_mensaje_respuesta;
				return;
			end if;
		end if;
		
		-- Se consulta si ya existe otro empleado con el tipo e identificacion
		begin
			select count(1)
			  into v_count_empleados
			  from empleados 
			 where tipo_identificacion		= p_tipo_identificacion
			   and identificacion			= p_identificacion
			   and id_empleado				!= p_id_empleado;
			
			if v_count_empleados  > 0 then
				s_mensaje_respuesta	:= 'Ya existe un empleado con el tipo de identificacion y la identificacion ingresada';
				return;
			end if;
		end;

		-- Se valida si se debe generar el email del empleado
		if (t_empleados.primer_apellido != p_primer_apellido 
		 or t_empleados.primer_nombre 	!= p_primer_nombre 
		 or t_empleados.id_pais 		!= p_id_pais ) then 
			pkg_empleados.prc_crear_email_empleado (p_primer_apellido			=> nvl(p_primer_apellido, t_empleados.primer_apellido)
												  , p_primer_nombre				=> nvl(p_primer_nombre, t_empleados.primer_nombre)
												  , p_id_pais					=> nvl(p_id_pais, t_empleados.id_pais)
												  , s_email						=> v_email
												  , s_mensaje_respuesta			=> s_mensaje_respuesta);
			if v_email is null then 
				return;
			end if;
		end if;
																 
		-- Se actualiza el empleado
		begin
			update empleados
			   set primer_apellido		= nvl(p_primer_apellido, primer_apellido)
				 , segundo_apellido		= nvl(p_segundo_apellido, segundo_apellido)
				 , primer_nombre		= nvl(p_primer_nombre, primer_nombre)
				 , otros_nombres		= nvl(p_otros_nombres, otros_nombres)
				 , id_pais				= nvl(p_id_pais, id_pais)
				 , tipo_identificacion	= nvl(p_tipo_identificacion, tipo_identificacion)
				 , identificacion		= nvl(p_identificacion, identificacion)
				 , email				= nvl(v_email, email)
				 , id_area				= nvl(p_id_area, id_area)
				 , fecha_ingreso		= nvl(p_fecha_ingreso, fecha_ingreso)
				 , fecha_modificacion	= systimestamp
			 where id_empleado 			= p_id_empleado;
			 s_mensaje_respuesta	:= 'Actualización del empleado exitosa';
			commit;
		exception
			when others then 
				s_mensaje_respuesta	:= 'Error al actualizar el empleado ' || nvl(p_primer_apellido, t_empleados.primer_apellido) || ' ' || nvl(p_primer_nombre, t_empleados.primer_nombre)|| '. Error: ' || sqlerrm;
				rollback;
				return;
		end;

	exception
		when others then 
			s_mensaje_respuesta	:= 'Error al actualizar el empleado ' || nvl(p_primer_apellido, t_empleados.primer_apellido) || ' ' || nvl(p_primer_nombre, t_empleados.primer_nombre)|| '. Error: ' || sqlerrm;
			rollback;
			return;
	end prc_editar_empleados;
	


	-- Procedimiento para editar un empleado --
	procedure prc_consulta_empleados (s_empleados						out sys_refcursor
								    , s_mensaje_respuesta				out varchar2) as
									
	begin
		open s_empleados for
			select a.id_empleado
				 , a.primer_apellido
				 , a.segundo_apellido
				 , a.primer_nombre
				 , a.otros_nombres
				 , a.id_pais
				 , a.nombre_pais
				 , a.tipo_identificacion
				 , a.desc_tipo_identificacion
				 , a.identificacion
				 , a.email
				 , a.fecha_ingreso
				 , a.id_area
				 , a.nombre_area
				 , a.cod_estado
				 , a.descripcion_estado
				 , a.fecha_registro
				 , a.fecha_modificacion
			from v_empleados				a
		order by primer_apellido
			   , segundo_apellido
			   , primer_nombre
			   , otros_nombres;
	exception
		when others then
			s_mensaje_respuesta := 'Error al consultar los empleados ' || sqlerrm;
			return;
	end prc_consulta_empleados;

	-- Procedimiento para empleados por parametros --
	procedure prc_consulta_empleados_parame (p_primer_apellido			in varchar2	default null
										   , p_segundo_apellido			in varchar2 default null
										   , p_primer_nombre			in varchar2 default null
										   , p_otros_nombres			in varchar2 default null
										   , p_tipo_identificacion		in varchar2 default null
										   , p_identificacion			in varchar2 default null
										   , p_id_pais					in number   default null
										   , p_email					in varchar2 default null
										   , p_cod_estado				in varchar2	default null
										   , s_empleados				out sys_refcursor
										   , s_mensaje_respuesta		out varchar2) as
									
	begin
		open s_empleados for
			select a.id_empleado
				 , a.primer_apellido
				 , a.segundo_apellido
				 , a.primer_nombre
				 , a.otros_nombres
				 , a.id_pais
				 , a.nombre_pais
				 , a.tipo_identificacion
				 , a.desc_tipo_identificacion
				 , a.identificacion
				 , a.email
				 , a.fecha_ingreso
				 , a.id_area
				 , a.nombre_area
				 , a.cod_estado
				 , a.descripcion_estado
				 , a.fecha_registro
				 , a.fecha_modificacion
			from v_empleados				a
		   where (a.primer_apellido 		like '%' || upper(p_primer_apellido)	|| '%'	or p_primer_apellido		is null)
		     and (a.segundo_apellido 		like '%' || upper(p_segundo_apellido)	|| '%'	or p_segundo_apellido		is null)
		     and (a.primer_nombre 			like '%' || upper(p_primer_nombre)		|| '%'	or p_primer_nombre			is null)
		     and (a.otros_nombres 			like '%' || upper(p_otros_nombres)		|| '%'	or p_otros_nombres			is null)
		     and (a.email 					like '%' || upper(p_email)				|| '%'	or p_email					is null)
		     and (a.tipo_identificacion 	= p_tipo_identificacion 						or p_tipo_identificacion	is null)
		     and (a.identificacion 			= p_identificacion	 							or p_identificacion			is null)
		     and (a.id_pais 				= p_id_pais 									or p_id_pais				is null)
		     and (a.cod_estado 				= p_cod_estado	 								or p_cod_estado				is null)
		order by primer_apellido
			   , segundo_apellido
			   , primer_nombre
			   , otros_nombres;
	exception
		when others then
			s_mensaje_respuesta := 'Error al consultar los empleados ' || sqlerrm;
			return;
	end prc_consulta_empleados_parame;

	-- Procedimiento para eliminar un empleado --
	procedure prc_eliminar_empleado (p_id_empleado						in empleados.email%type
									, s_mensaje_respuesta				out varchar2) as

	t_empleados							empleados%rowtype;
	
	begin
		-- Se valida si el empleado ingresado existe
		begin 
			select *
			  into t_empleados
			  from empleados
			 where id_empleado = p_id_empleado;
		exception
			when no_data_found then 
				s_mensaje_respuesta	:= 'No se encontro información para el empleado ingresado';
				return;
			when others then
				s_mensaje_respuesta	:= 'Error al consultar el empleado ' || sqlerrm;
				return;
		end;							  
		
		begin
			delete from empleados where id_empleado = p_id_empleado;
			s_mensaje_respuesta	:= 'Empleado eliminado exitosamente';
			commit;
		exception
			when others then 
				s_mensaje_respuesta	:= 'Error al eliminar el empleado ' || t_empleados.primer_apellido	|| ' '
																		|| t_empleados.primer_nombre 	|| ' ' || sqlerrm;
		end;
	end prc_eliminar_empleado;

end pkg_empleados;
/